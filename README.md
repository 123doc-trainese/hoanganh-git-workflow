# Bài tập Git - Workflow

**Git - workflow* .
### Lý thuyết
- Tài liệu: Git - Documentation
- Kiến thức cần nắm được
  git là gì, tại sao phải sử dụng git.
  cách làm việc nhóm. Chịu trách nhiệm trên từng commit
### Thực hành 
#### Basic
- Tạo 1 repo mới trên gitlab trong group 123doc-trainese đã được add vào
- Chia thành develop và master
- Remote và up 1 source code lên develop
- Tạo các commit theo chuẩn Conventional Commit
- Merge nhánh develop vào master
- Viết file README.md Lý thuyết, Tham khảo
### Advance
xử lý conflict
revert commit
cherry pick commit
reset commit
squash commit

### Kiến thức nắm được
- Nắm được kiến thức git cơ bản

- Xử lý conflict:
xảy ra khi 1 hoặc nhiều lập trình viên cùng sửa trên một dòng code.
Xử lý: trong trường hợp 1 trong 2 ltv code sai thì chọn 1 trong 3 hướng giải quyết: quay về code gốc hoặc tiếp tục theo code của 1 ltv. 
trường hợp cả 2 ltv đúng thì gộp code của cả 2.
- cherry pick commit: chỉ định lấy 1 hoặc nhiều commit từ brand khác về nhánh hiện tại đang trỏ tới

- revert commit: hoàn tác lại một commit không làm ảnh hưởng tới commit khác

- reset commit: di chuyển con trỏ HEAD về commit được chọn và xóa các commit tiếp theo của nó

- squash commit: gộp nhiều commit thành 1 commit 

Thực hiện bởi [Nguyen Hoang Anh](https://github.com/anhbk177)

## Liên kết

[hoanganh-git-workflow](https://github.com/anhbk177/hoanganh-git-workflow





